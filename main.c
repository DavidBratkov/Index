/*
David Bratkov

11/14/2019

CSE 224

This is a function in which it converts any lowercase into uppercase then converts all extra whitespace into a single whitespace then detects phrases and then indexes it and stores it into an index array
*/

#include <stdio.h>
#include <string.h>

int convert();//My super duper get char function prototype with whitespace removal

void main() {

int i=0, x=0, j=1, y=0, d=0, arrayspot=0;
char dictionary [1000][201];
int count[1000];
char temp[201];
int retchar;
_Bool first=0;

retchar=convert();

while (retchar != EOF && i < 1000) {//The loop that puts the phrase into the dictionary
x=0;
first=0;
	while (retchar == 32 && first != 1) { //removes all the spaces at the beggining
	retchar=convert();
	}
first=1;
	while (retchar != EOF && x < 200 && retchar != ',' && retchar != '.' && retchar != ';' && retchar != ':' && retchar != '?' && retchar != '!') {//The loop that stores the letters into the phrase
	//printf("retchar:%c x:%d\n",retchar,x);
	temp[x]=retchar;
	x++;
	retchar=convert();
	}
//printf("retchar:%c before x:%d\n", retchar,x);
	if (retchar == ',' || retchar == '.' || retchar == ';' || retchar == ':' || retchar == '?' || retchar == '!') {//The loop that starts a new phrase
	temp[x]='\0';
	retchar=convert();
	}
//printf("retchar:%c after x:%d\n",retchar,x);
/*
d=0;
printf("phrase:");
while (temp[d] != '\0'){
printf("%c",temp[d]);
d++;
}
printf("\n");
*/
int compare=strcmp(dictionary[y],temp);
int done=0;
//printf("compare:%d\n",compare);
y=0;
        while (y < 1000 && retchar != EOF){//the loop that checks if there is already a copy of the phrase in the dictionary array
            compare=strcmp(dictionary[y],temp);
		if (compare == 0){
//		printf("y:%d arrayspot:%d\n",y,arrayspot);
                count[y]++;
                done=1;
//		printf("added to existing phrase");
                }
        y++;
        }
//	printf("done:%d",done);
         if (done != 1 && retchar != EOF){//the loop that adds a new phrase into the array
                strcpy(dictionary[arrayspot],temp);
                count[arrayspot]=1;
//		printf("added a new spot");
                arrayspot++;
          }
/*
d=0;
printf("dict phrase:");
while (dictionary[i][d] != '\0'){
printf("%c",dictionary[i][d]);
d++;
}
printf("\n");
*/

i++;
//printf("DEBUG x=%d, i=%d, retchar=%c\n",x,i,retchar);
}

//printf("count at 0:%d",count[0]);
/*
d=0;
while (d < i) {
printf("This is phrase #%d it is:",d);
x=0;
while (dictionary[d][x] != '\0'){
printf("%c",dictionary[d][x]);
x++;
}
printf(" The count for this phrase is %d\n",count[d]);
d++;
}
*/
int z=arrayspot;
int n, m, bub=arrayspot;
//printf("i:%d\n",i);


for (j=0;j<(bub-1);j++) {
for (i=0;i<(bub-1);i++) {//Bubble Sort for the length of i
char temp1[201];
n=strlen(dictionary[i]);
m=strlen(dictionary[i+1]);
	if (n > m) {
	strcpy(temp1,dictionary[i]);
	strcpy(dictionary[i],dictionary[(i+1)]);
	strcpy(dictionary[(i+1)],temp1);
	int temp2=count[i];//This changes where the count is located to match the change in dictionary
	count[i]=count[(i+1)];
	count[(i+1)]=temp2;
	}
}
}
int r=0;
while (z > r) {//This is the function that prints the dictionary off to the screen
printf("%.4d <",count[r]);
x=0;
	while (dictionary[r][x] != '\0') {
	printf("%c",dictionary[r][x]);
	x++;
	}
printf(">\n");
r++;
}

}
